/* -Метод об'єкту - це функція, яка є властивістю об'єкту і може бути викликана за допомогою імені 
    об'єкту та оператора крапки. 
   - Будь-який тип даних може бути властівістью обʼєета 
   - Об'єкт - це посилальний тип даних, що означає, що змінні, які містять об'єкти, фактично містять 
   посилання на місце в пам'яті, де зберігається об'єкт. 
    */



let createNewUser = function() {
    let name = prompt("Введіть ім'я");
    let surname = prompt("Введіть прізвище");

    let newUser= {
        firstName: name,
        lastName: surname,
        getLogin (){
            const firstLetter = this.firstName.charAt(0).toLowerCase();
            const login = firstLetter + this.lastName.toLowerCase();
        return login;
        }
    }
    return newUser;
}

const user = createNewUser();

console.log(user.getLogin());